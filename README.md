# EDAF75, project report

This is the report for

 + Tom Richter, `Tfy14tri`

I solved this project on my own, except for some help from William Lindskog in the early stages before he decided to drop out of the course.


## ER-design

The model is in the file [`er-model.png`](er-model.png):

<center>
    <img src="er-model.png" width="100%">
</center>

The image above describes our model for Krusty Kookies Sweden AB's production and delivery system.


## Relations

The ER-model above gives the following relations (neither
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nor [HTML5](https://en.wikipedia.org/wiki/HTML5) handles
underlining withtout resorting to
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
so we use bold face for primary keys, italicized face for
foreign keys, and bold italicized face for attributes which
are both primary keys and foreign keys):

+ ingredients(**ingredient**, quantity, unit)
+ cookieIngredients(**_cookieName_**, **ingredient**, quantity, unit)
+ cookies(**cookieName**)
+ pallets(**palletId**, _cookieName_, _orderId_, isBlocked, productionDate, currentState, deliveryTime)
+ orderDetails(**_cookieName_**, **_orderId_**, nbrOfPallets)
+ orders(**orderId**, **_customerId_**, orders_to_deliver)
+ customers(**customerId**, customerName, address)
+ ingredientDeliveries(**deliveryId**, **_ingredient_**, quantity, deliveryTime)


## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`krusty.sql`](krusty.sql) (defines the tables)

So, to create and initialize the database, we run:

```shell
sqlite3 krusty.sqlite < krusty.sql
```

(or whatever you call your database file).

## How to compile and run the program abc

This section should give a few simple commands to type to
compile and run the program from the command line, such as:

```shell
python3 -m venv krusty-kookies-sweden-ab/
cd krusty-kookies-sweden-ab/
. bin/activate
pip install requests
pip install fastAPI
pip install uvicorn

*put krusty.sql, krusty-api.py, check-krusty.py in the folder krusty-kookies-sweden-ab*

python krusty-apy.py
python check-krusty.py
```

DROP TABLE IF EXISTS pallets;
DROP TABLE IF EXISTS cookies;
DROP TABLE IF EXISTS cookieIngredients;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS orderDetails;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS ingredientDeliveries;

PRAGMA foreign_keys = ON;

CREATE TABLE pallets(
      palletId          TEXT DEFAULT (lower(hex(randomblob(16)))),
      isBlocked         INT   DEFAULT     0,
      productionDate    Date    DEFAULT     CURRENT_DATE,
      currentState      TEXT    DEFAULT     'Go',
      deliveryTime      Time    DEFAULT     NULL,
      cookieName        TEXT,
      orderNbr          TEXT    DEFAULT     NULL,
      PRIMARY KEY       (palletId),
      FOREIGN KEY       (cookieName)        REFERENCES cookies(cookieName),
      FOREIGN KEY       (orderNbr)          REFERENCES orders(orderNbr)
          );

CREATE TABLE cookies(
      cookieName        TEXT,
      PRIMARY KEY  (cookieName)
          );

CREATE TABLE customers(
      customerId        TEXT DEFAULT (lower(hex(randomblob(16)))),
      address           TEXT,
      customerName      TEXT,
      PRIMARY KEY       (customerId)
          );

CREATE TABLE ingredients(
      ingredient        TEXT,
      totalQuantity     DEC DEFAULT 0,
      unit              TEXT,
      PRIMARY KEY       (ingredient)
          );

CREATE TABLE ingredientDeliveries(
      deliveryId    TEXT DEFAULT (lower(hex(randomblob(16)))),
      ingredient    TEXT,
      quantity      DEC,
      deliveryTime  DATETIME DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (deliveryId),
      FOREIGN KEY (ingredient) REFERENCES ingredients(ingredient)
);


CREATE TABLE orders(
      orderId TEXT,
      customerId TEXT,
      PRIMARY KEY (orderId),
      FOREIGN KEY (customerId) REFERENCES customers(customerId) ON DELETE SET NULL
          );


CREATE TABLE cookieIngredients(
      ingredient        TEXT,
      amount            INT,
      cookieName        TEXT,
      PRIMARY KEY       (cookieName, ingredient),
      FOREIGN KEY       (ingredient)     REFERENCES ingredients(ingredient),
      FOREIGN KEY       (cookieName)     REFERENCES cookies(cookieName) ON DELETE CASCADE
          );

CREATE TABLE orderDetails(
      orderNbr          TEXT,
      cookieName        TEXT,
      nbrOfPallets      INT,
      PRIMARY KEY (orderNbr, cookieName),
      FOREIGN KEY       (cookieName)          REFERENCES cookies(cookieName),
      FOREIGN KEY       (orderNbr)            REFERENCES orders(orderNbr) ON DELETE CASCADE
          );


DROP TRIGGER IF EXISTS update_ingredients;
CREATE TRIGGER update_ingredients
BEFORE INSERT ON pallets
BEGIN
    UPDATE ingredients
    SET totalQuantity = totalQuantity - 54*(
        SELECT amount
        FROM cookieIngredients
        WHERE cookieIngredients.ingredient = ingredients.ingredient AND cookieIngredients.cookieName=NEW.cookieName
    )
    WHERE ingredient IN (
      SELECT ingredient
      FROM cookieIngredients
      WHERE cookieName = NEW.cookieName
    );
END;


DROP TRIGGER IF EXISTS checkIngredients;
CREATE TRIGGER checkIngredients
AFTER UPDATE OF totalQuantity ON ingredients
    WHEN NEW.totalQuantity < 0
    BEGIN 
        SELECT RAISE (ROLLBACK, "Not enough ingredients");
END;


-- UPDATE ingredients
--     SET totalQuantity = totalQuantity - 54*(
--         SELECT amount
--         FROM cookieIngredients
--         WHERE cookieIngredients.ingredient = ingredients.ingredient AND cookieIngredients.cookieName="Almond delight"
--     )
--     WHERE ingredient IN (
--       SELECT ingredient
--       FROM cookieIngredients
--       WHERE cookieName = "Almond delight"
--     );


-- UPDATE    ingredients
    -- SET       totalQuantity = totalQuantity - 54*cI.amount
    -- FROM (SELECT cookieName, amount FROM cookieIngredients) AS cI
    -- WHERE cI.cookieName = NEW.cookieName
-- -- with (select ) as temp
-- update ingredients 
-- set totalQuantity = 1
-- where cookieIngredients.cookieName = "Almond delight";

-- -- UPDATE ingredients 
-- -- SET totalQuantity = totalQuantity - cI.blah 
-- -- FROM (SELECT 1 AS blah FROM cookieIngredients) AS cI 
-- -- WHERE ingredient = cI.ingredient AND cI.cookieName = "Almond delight";


-- -- UPDATE ingredients
-- -- SET totalQuantity = totalQuantity - (
-- --     SELECT amount
-- --     FROM cookieIngredients
-- --     WHERE cookieIngredients.ingredient = ingredients.ingredient
-- -- )
-- -- WHERE ingredient IN (
-- --   SELECT ingredient
-- --   FROM cookieIngredients
-- --   WHERE cookieName = "Almond delight"
-- -- );



-- UPDATE table
-- SET column_1 = new_value_1,
--     column_2 = new_value_2
-- WHERE
--     search_condition 
-- ORDER column_or_expression
-- LIMIT row_count OFFSET offset;


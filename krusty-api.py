from fastapi import FastAPI, Request
import uvicorn
import sqlite3
import json
from datetime import date
from starlette.responses import Response
from urllib.parse import quote, unquote


app=FastAPI()

dbname="krusty.sqlite"

@app.post("/reset", status_code=205)
async def reset(request: Request):
	with sqlite3.connect(dbname) as db:
		deleteOrders = "DELETE FROM orders"
		deletePallets = "DELETE FROM pallets"
		deleteCookies = "DELETE FROM cookies"
		deleteIngredients = "DELETE FROM ingredients"
		deleteCustomers = "DELETE FROM customers"
		c=db.cursor()
		c.execute(deletePallets)
		c.execute(deleteCookies)
		c.execute(deleteIngredients)
		c.execute(deleteCustomers)
		c.execute(deleteOrders)
		db.commit()
		return Response(content = "{ \"location\": \"/\" }", status_code = 205)


@app.post("/customers")
async def create_customer(request: Request):
	with sqlite3.connect(dbname) as db:
		customer = await request.json()
		c=db.cursor()
		c.execute(
			"""
			INSERT
			INTO    customers(customerName, address)
			VALUES  (?, ?)
			""",
			[customer['name'], customer['address']]
		)
		db.commit()
		url_encoded_title=quote(customer['name'])
		resp = "{{ \"location\": \"/customers/{}\" }}".format(url_encoded_title)
		print(resp)
		return Response(content = resp, status_code = 201)


# @app.get("/customers")
# async def get_customer(request: Request):
# 	with sqlite3.connect(dbname) as db:
# 		db.commit()
# 	    return Response(content = "/customers/{}".format(customer[customerName]), status_code = 200)


@app.post("/ingredients")
async def create_ingredient(request: Request):
	with sqlite3.connect(dbname) as db:
		ingredient = await request.json()
		c=db.cursor()
		c.execute(
			"""
			INSERT
			INTO    ingredients(ingredient, unit)
			VALUES  (?, ?)
			""",
			[ingredient['ingredient'], ingredient['unit']]
		)
		db.commit()
		url_encoded_title=quote(ingredient['ingredient'])
		found="/ingredients/{}".format(url_encoded_title)
		return Response(content = json.dumps({"location": found}, indent = 4), status_code = 201)

@app.post("/ingredients/{ingredient}/deliveries")
async def create_delivery(ingredient : str, request: Request):
	print(ingredient)
	with sqlite3.connect(dbname) as db:
		delivery = await request.json()
		print(delivery)
		insertDelivery = """ 
			INSERT 
			INTO ingredientDeliveries(ingredient, deliveryTime, quantity)
			VALUES (?, ?, ?)
						"""
		updateIngredient = """
						 UPDATE ingredients
						 SET totalQuantity = totalQuantity + ?
						 WHERE ingredient = ?
						 """
		c = db.cursor()
		c.execute(insertDelivery, [ingredient, delivery['deliveryTime'], delivery['quantity']])
		c.execute(updateIngredient, [delivery['quantity'], ingredient])
		db.commit()
		c.execute("""
				SELECT  ingredient, totalQuantity, unit
						FROM    ingredients
						WHERE 	ingredient=?
				""", [ingredient])

		updatedIngredient = [row for row in c][0]
		(ingredient, totalQuantity, unit) = updatedIngredient
		data = {
			"data": {
				"ingredient": ingredient,
				"quantity": totalQuantity,
				"unit": unit
			}
		}
		found = json.dumps(data, indent = 4)
		return Response(content = json.dumps(data, indent=4), status_code = 201)


@app.get("/ingredients")
async def get_ingredients():
	with sqlite3.connect("krusty.sqlite") as db:
		query="""SELECT ingredient, totalQuantity, unit FROM ingredients"""
		c=db.cursor()
		c.execute(query)
		found = [{"ingredient": ingredient, "quantity": totalQuantity, "unit" : unit} 
				for ingredient, totalQuantity, unit in c]
		return Response(content = json.dumps({"data": found}, indent = 4), status_code = 200)

@app.post("/cookies")
async def create_cookies(request: Request):
	with sqlite3.connect("krusty.sqlite") as db:
		recipe = await request.json()
		c=db.cursor()
		c.execute("""SELECT cookieName FROM cookies""")
		print([{"cookieName": cookieName} for cookieName in c])
		if recipe["name"] not in [cookieName for cookieName in c]:
			insertCookie = """ 
							INSERT 
							INTO cookies(cookieName)
							VALUES (?)
							"""
			c.execute(insertCookie, [recipe["name"]])
		print(recipe)
		recipe["recipe"] #hela listan med ingredienser, ie recipe["recipe"] ger lista av dictionaries. Varje dictionary är på formen {'ingredient': 'Butter', 'amount': 200}
		for i in range(len(recipe["recipe"])):
			insertCookieIngredient = """
						 INSERT
						 INTO cookieIngredients(ingredient, amount, cookieName)
						 VALUES (?,?,?)
						 """
			c.execute(insertCookieIngredient, [recipe["recipe"][i]['ingredient'], recipe["recipe"][i]['amount'], recipe["name"]])
		db.commit()
		url_encoded_title=quote(recipe["name"])
		resp = "{{ \"location\": \"/cookies/{}\" }}".format(url_encoded_title)
		return Response(content = resp, status_code = 201)
		# return Response(content = 'pong', status_code = 200)



@app.get("/cookies")
async def get_cookies():
	with sqlite3.connect(dbname) as db:
		c = db.cursor()
		c.execute(
			"""
			SELECT	cookieName, coalesce(count(),0) AS nrPallets
			FROM	cookies
			LEFT JOIN pallets
			USING (cookieName)
			GROUP BY cookieName
			""")
		found = {"data":[{"name": cookieName, "pallets" : nrPallets}
			for (cookieName, nrPallets) in c]}
		return Response(content = json.dumps(found, indent = 4), status_code = 200)

@app.get("/cookies/{cookieName}/recipe")
async def get_specific_cookie(cookieName: str):
	with sqlite3.connect(dbname) as db:
		query="""SELECT ingredient, amount, unit FROM cookieIngredients JOIN ingredients USING (ingredient) WHERE cookieName = ?"""
		c=db.cursor()
		c.execute(query, [cookieName])
		found = [{"ingredient": ingredient, "amount": amount,  "unit": unit} 
				for (ingredient, amount,  unit) in c]
		return Response(content = json.dumps({"data": found}, indent = 4), status_code = 200)	

@app.post("/pallets")
async def create_pallet(request: Request):
	with sqlite3.connect(dbname) as db:
		try:
			pallet = await request.json()
			query="""INSERT
					INTO pallets(cookieName)
					VALUES (?) """
			c=db.cursor()
			c.execute(query, [pallet["cookie"]])
			palletIdquery="""SELECT palletId FROM pallets WHERE rowid = last_insert_rowid() """
			c.execute(palletIdquery)
			palletId=c.fetchone()[0]
			db.commit()
			resp={"location": "/pallets/{}".format(palletId)}
			return Response(content = json.dumps(resp, indent = 4), status_code = 201)
		except sqlite3.Error:
			return Response(content=json.dumps({"location": ""}, indent = 4), status_code=422)


@app.get("/pallets")
async def get_pallets(cookie: str = None, before: str = None, after: str = None):
	print('GET PALLETS', cookie, before, after)
	with sqlite3.connect(dbname) as db:
		c=db.cursor()
		# if unquoteCookie == '':
		# 	newCookie = '%'
		# else:
		# 	newCookie = unquoteCookie
		# if before == '':
		# 	newBefore = '0000-01-01'
		# else:
		# 	newBefore = before
		# if after == '':
		# 	newAfter = date.today().isoformat()
		# else: 
		# 	newAfter = after
		# if before == '' and after == '':
		# 	newBefore = date.today().isoformat()
		# 	newAfter = '0000-01-01'
		query = """SELECT palletID, cookieName, productionDate, isBlocked FROM pallets"""
		where_clauses = []
		sql_params = []
		if cookie:
			where_clauses.append('cookieName = ?')
			#unquoteCookie = unquote(cookie)
			sql_params.append(cookie)
		if before:
			where_clauses.append('productionDate < date(?)')
			sql_params.append(before)
		if after:
			where_clauses.append('productionDate > date(?)')
			sql_params.append(after)

		if len(where_clauses) > 0:
			query += ' WHERE ' + ' AND '.join(where_clauses)

		#query = """ SELECT palletID, cookieName, productionDate, isBlocked FROM pallets WHERE cookieName LIKE ? AND (productionDate < date(?)  productionDate > date(?)) """
		c.execute(query, sql_params)
		data = [{"id": palletId, "cookie": cookieName,  "productionDate": productionDate, "blocked": isBlocked} for (palletId, cookieName, productionDate, isBlocked) in c]
		content={"data": data}
		db.commit()
		return Response(content = json.dumps(content, indent = 4), status_code = 200)

		
		# if after != '':
		# 	afterquery = """ SELECT palletID, cookieName, productionDate, isBlocked FROM pallets WHERE cookieName = ? AND productionDate > date(?) """
		# if before != '':
		# 	beforequery = """ SELECT palletID, cookieName, productionDate, isBlocked FROM pallets WHERE cookieName = ? AND productionDate < date(?) """
		# 	c.execute(query, [cookie, before])
@app.post("/cookies/{cookieName}/block")
async def block_cookie(cookieName: str, after: str = None, before: str = None):
	with sqlite3.connect(dbname) as db:
		c=db.cursor()
		#unquoteCookie = unquote(cookieName)
		query="""UPDATE pallets SET isBlocked = 1 WHERE cookieName = ?"""
		sql_params=[]
		where_clauses = []
		if after:
			where_clauses.append('productionDate > date(?)')
			sql_params.append(after)
		if before:
			where_clauses.append('productionDate < date(?)')
			sql_params.append(before)
		if len(where_clauses)>0:
			query += ' AND ' + ' AND '.join(where_clauses)
		c.execute(query, [cookieName]+sql_params)
		db.commit()
		return Response(content = "", status_code=205)

@app.post("/cookies/{cookieName}/unblock")
async def unblock_cookie(cookieName: str, after: str = None, before: str = None):
	with sqlite3.connect(dbname) as db:
		c=db.cursor()
		unquoteCookie = unquote(cookieName)
		query="""UPDATE pallets SET isBlocked = 0 WHERE cookieName = ?"""
		sql_params=[]
		where_clauses = []
		if after:
			where_clauses.append('productionDate > date(?)')
			sql_params.append(after)
		if before:
			where_clauses.append('productionDate < date(?)')
			sql_params.append(before)
		if len(where_clauses)>0:
			query += ' AND ' + ' AND '.join(where_clauses)
		c.execute(query, [unquoteCookie]+sql_params)
		db.commit()
		return Response(content = "", status_code=205)


if __name__ == "__main__":
	uvicorn.run("krusty-api:app", host="localhost", port=8888, log_level="info")





